const problem2 = require('../problem2');
const path = require('path');

problem2(path.join(__dirname, "../lipsum.txt"), (err, data) => {
    if (err)
        console.log(err);
    else {
        console.log(data);
    }
})