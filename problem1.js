const fs = require('fs');
const path = require('path');

module.exports = function problem(dirName, noOfFile, cb) {
    if (typeof cb == 'function') {
        if (typeof dirName == 'string') {
            fs.mkdir(dirName, { recursive: true }, (err) => {
                if (err) {
                    cb(err);
                }
                else {

                    let counter = 0;
                    let counterInner = 0;
                    let errorArray = [];
                    let successArray = [];
                    for (let index = 0; index < noOfFile; index++) {
                        setTimeout(() => {
                            let filePath = path.join(__dirname, dirName, "/file" + index + ".json");
                            fs.writeFile(filePath, JSON.stringify({ name: "Deepanshu" }), (err) => {
                                if (err) {
                                    if (counter >= noOfFile - 1) {
                                        // console.log(err);
                                        if (successArray.length >= 1) {
                                            cb(errorArray, successArray)
                                        }
                                        else {
                                            cb(errorArray);
                                        }

                                    }
                                    else {
                                        counter += 1;
                                        counterInner += 1;
                                        errorArray.push(err.message);
                                    }
                                }
                                else {
                                    counter += 1;
                                    successArray.push(filePath + 'Created');
                                    // console.log(filePath + 'Created\n' + counter)
                                    setTimeout(() => {

                                        fs.unlink(filePath, (err) => {
                                            if (err) {
                                                if (counterInner == noOfFile - 1) {
                                                    if (successArray.length >= 1) {
                                                        cb(errorArray, successArray)
                                                    }
                                                    else {
                                                        cb(errorArray);
                                                    }

                                                }
                                                else {
                                                    counterInner += 1;
                                                    errorArray.push(err.message);
                                                }

                                            }
                                            else {
                                                counterInner += 1;
                                                if (counterInner == noOfFile) {
                                                    successArray.push(filePath + 'Deleted')
                                                    if (errorArray.length >= 1) {
                                                        cb(errorArray, successArray);

                                                    }
                                                    else {
                                                        cb(null, successArray);
                                                        // console.log(filePath + 'Deleted\n')

                                                    }
                                                }
                                                else {
                                                    successArray.push(filePath + 'Deleted');
                                                }
                                            }
                                        });
                                    }, 2000);
                                }
                            });
                        }, 2000);
                    }
                }

            });
        }
    }
    else {
        console.error(new Error("Cb function not passed"));
    }
}