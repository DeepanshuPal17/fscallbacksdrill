const fs = require('fs');
const path = require('path');


module.exports = function problem2(filePath, cb) {
    fs.readFile(filePath, 'utf-8', (err, data) => {
        if (err) {
            cb(err);
        }
        else {
            let fileDataInUpperCase = data.toUpperCase();
            let fileName1 = "file1.txt";
            fs.writeFile(path.join(__dirname, "Output/" + fileName1), fileDataInUpperCase, (err) => {
                if (err) {
                    cb(err);
                }
                else {
                    fs.writeFile(path.join(__dirname, "Output/filenames.txt"), fileName1, (err) => {
                        if (err) {
                            cb(err);
                        }
                        else {

                            fs.readFile(path.join(__dirname, "Output/" + fileName1), 'utf-8', (err, data) => {
                                if (err) {
                                    cb(err);
                                }
                                else {
                                    let newFileData = data.toLowerCase();
                                    newFileData = newFileData.split('.');
                                    // console.log(newFileData);
                                    let fileName2 = "file2.txt";
                                    fs.writeFile(path.join(__dirname, "Output/" + fileName2), JSON.stringify(newFileData), (err) => {
                                        if (err) {
                                            cb(err);
                                        }
                                        else {
                                            fs.appendFile(path.join(__dirname, "Output/filenames.txt"), "\n" + fileName2, (err) => {
                                                if (err) {
                                                    cb(err);
                                                }
                                                else {
                                                    fs.readFile(path.join(__dirname, "Output/" + fileName2), 'utf-8', (err, data) => {
                                                        if (err) {
                                                            cb(err);
                                                        }
                                                        else {
                                                            let sortFileDataArray = JSON.parse(data);
                                                            sortFileDataArray.sort();
                                                            let fileName3 = "file3.txt";
                                                            fs.writeFile(path.join(__dirname, "Output/" + fileName3), JSON.stringify(sortFileDataArray), (err) => {
                                                                if (err) {
                                                                    cb(err);
                                                                }
                                                                else {
                                                                    fs.appendFile(path.join(__dirname, "Output/filenames.txt"), "\n" + fileName3, (err) => {
                                                                        if (err) {
                                                                            cb(err);
                                                                        }
                                                                        else {
                                                                            fs.readFile(path.join(__dirname, "Output/filenames.txt"), 'utf-8', (err, data) => {
                                                                                if (err) {
                                                                                    cb(err);
                                                                                }
                                                                                else {
                                                                                    let fileNames = data.split('\n');
                                                                                    let count = 0;
                                                                                    fileNames.forEach((fileName) => {
                                                                                        fs.unlink(path.join(__dirname, "Output/" + fileName), (err) => {
                                                                                            if (err) {
                                                                                                cb(err);
                                                                                            }
                                                                                            else {
                                                                                                count += 1;
                                                                                                if (count == fileNames.length) {
                                                                                                    cb(null, "success");
                                                                                                }
                                                                                            }
                                                                                        })
                                                                                    })
                                                                                }
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            })

                                                        }
                                                    });
                                                }
                                            })
                                        }
                                    });

                                }
                            })

                        }
                    });
                }
            });
        }
    })

}